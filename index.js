const Discord = require('discord.js');
const client = new Discord.Client();

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
  client.user.setActivity(';roll', { type: 'WATCHING' });
});

client.on('message', msg => {
  let results = [];

  if (msg.content.startsWith(';roll')) {
    if(msg.content.match(/;roll\ *\d+\ *d\ *\d+/) == undefined) {
      msg.channel.send( "Exemple ``;roll 1d10``");
    }
    else {
      let args = msg.content.match(/(\d+)\ *d\ *(\d+)/);

      if(parseInt(args[1], 10) < 1 || parseInt(args[1], 10) > 200) {
        msg.channel.send( "Number of roll must be between 1 and 200");
        return;
      }

      if(parseInt(args[2], 10) < 2 || parseInt(args[2], 10) > 200) {
        msg.channel.send( "Dice must be between 2 and 200");
        return;
      }

      for(let i = 0; i < parseInt(args[1], 10); i++) {
        results.push(getRandomInt(parseInt(args[2], 10)));
      }


      msg.channel.send( 'Rolled ``' + args[1] + 'd' + args[2] + '`` Result: ``[' + results.join(', ') + ']``');
    }
  }
});

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max)) + 1;
}

client.login('TOKEN');